﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LoginPage.Models;
using BusinessLayer;
using System.Web.Security;

namespace LoginPage.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            //test for bitbukcet source tt
            //test for bitbukcet source sd
            //test for bitbukcet source 23 manoj test by naresh
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User u)
        {
            UserRegistration ur = new UserRegistration();
            if (ModelState.IsValid)
            {
                using (MyDataBaseEntitiesRegistration dc = new MyDataBaseEntitiesRegistration())
                {
                    //var v = dc.Users.Where(x => x.Username.Equals(u.Username) && x.Password.Equals(u.Password)).FirstOrDefault();
                    var v = dc.UserRegistrations.Where(x => x.Username.Equals(u.Username) && x.Password.Equals(u.Password)).FirstOrDefault();
                    if (v != null)
                    {
                        Session["LoginUserID"] = v.UserID.ToString();
                        Session["LoginUserFullName"] = v.Username.ToString();
                        return RedirectToAction("Authentication");
                    }
                    else
                    {
                        ViewBag.Message = "UserName or Password is incorrect !!";
                    }
                }
            }
            return View();
        }

        public ActionResult Authentication()
        {
            if (Session["LoginUserID"] != null)
            {
                return View();
            }
            else
            {
                //return JavaScript("confirm('Wrong Credentials Entered')");
                return RedirectToAction("Login");
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return View();
        }

        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [ActionName("Registration")]
        public ActionResult RegistrationPost(UserRegistration regBLL)
        {
            if (ModelState.IsValid)
            {
                using (MyDataBaseEntitiesRegistration mdeg = new MyDataBaseEntitiesRegistration())
                {
                    try
                    {
                        mdeg.UserRegistrations.Add(regBLL);
                        mdeg.SaveChanges();
                        ModelState.Clear();
                        regBLL = null;
                        ViewBag.Message = "Successfully Registration";
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "An error occurred while updating the entries. See the inner exception for details.")
                        {
                            Response.Write("<a style=\"color:red\">User Id not availabe !! . Please Choose different ID</a>");
                        }
                    }
                }
            }
            return View();
        }
    }
}